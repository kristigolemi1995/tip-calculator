## Name
# Tip-calculator

## Description
This simple project calculates the cash amount each guest should pay based on the receipt, chosen percentage of the tip by the user and the total number of the guests.

## Authors and acknowledgment
This project was given as a task in 100 Days of Code: The Complete Python Pro Bootcamp and solved by Kristi Golemi.
